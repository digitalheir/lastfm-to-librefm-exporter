import {md5hash} from "../md5";
import {LibreScrobbleTrack} from "../librefm";
import {makePostRequest} from "../fetch-url";
import {urlEncodeParams} from "../util/collections";

export const urlGetToken = (base_url: string, api_key: string, secret: string) => {
    const params = [
        ["method", "auth.getToken"],
        ["api_key", api_key],
        // ["format", "json"],
    ];
    return `${base_url}?${urlEncodeParams(params)}&api_sig=${constructSignatureForParams(params, secret)}&format=json`;
};

export const connectApplication = (baseUrl: string,
                                   apiKey: string,
                                   cb?: string) => `${baseUrl}auth/?api_key=${cb ? apiKey + "&cb=" + encodeURIComponent(cb) : apiKey}`;

// export const authorizeToken = (baseUrl: string,
//                                apiKey: string,
//                                token: string) => `${baseUrl}auth/?api_key=${apiKey}&token=${token}`;

export const createSessionUrl = (baseUrl: string,
                                 apiKey: string,
                                 token: string,
                                 secret: string) => {
    const params = [["api_key", apiKey], ["method", "auth.getSession"], ["token", token],
        // ["format", "json"]
    ];
    return `${baseUrl}?${urlEncodeParams(params)}&api_sig=${constructSignatureForParams(params, secret)}&format=json`;
};

const append = function (data: string[][], key: string, value?: string) {
    if (value) data.push([key, value]);
};

const appendTrack = (data: string[][],
                     track: LibreScrobbleTrack,
                     i: number) => {
    append(data, `artist[${i}]`, track.artist); //  (Required) : The artist name.
    append(data, `track[${i}]`, track.track); //  (Required) : The track name.
    append(data, `timestamp[${i}]`, track.timestamp.toString()); //  (Required) : The time the track started playing, in UNIX timestamp format (integer number of seconds since 00:00:00, January 1st 1970 UTC). This must be in the UTC time zone.
    append(data, `album[${i}]`, track.album); //  (Optional) : The album name.
    // append(data, `context[${i}]`, track.context); //  (Optional) : Sub-client version (not public, only enabled for certain API keys)
    // append(data, `streamId[${i}]`, track.streamId); //  (Optional) : The stream id for this track received from the radio.getPlaylist service, if scrobbling Last.fm radio
    // append(data, `chosenByUser[${i}]`, track.chosenByUser); //  (Optional) : Set to 1 if the user chose this song, or 0 if the song was chosen by someone else (such as a radio station or recommendation service). Assumes 1 if not specified
    append(data, `trackNumber[${i}]`, track.trackNumber); //  (Optional) : The track number of the track on the album.
    append(data, `mbid[${i}]`, track.mbid); //  (Optional) : The MusicBrainz Track ID.
    // append(data, `albumArtist[${i}]`, track.albumArtist); //  (Optional) : The album artist - if this differs from the track artist.
    append(data, `duration[${i}]`, track.duration); //  (Optional) : The length of the track in seconds.
};

export function createScrobbleFormData(tracks: LibreScrobbleTrack[], apiKey: string, sk: string, secret: string) {
    const data: string[][] = [];
    tracks.forEach((track, i) => appendTrack(data, track, i));
    const method = "track.scrobble";
    data.push(["method", method]);
    data.push(["api_key", apiKey]);
    data.push(["sk", sk]); // A session key generated by authenticating a user via the authentication protocol.
    const formData = new FormData();
    data.forEach((pair) => formData.append(pair[0], pair[1]));
    formData.append("api_sig", constructSignatureForParams(data, secret));
    return formData;
}

export const scrobbleTracks = (baseUrl: string,
                               apiKey: string,
                               sk: string,
                               secret: string,
                               tracks: LibreScrobbleTrack[]) => {
    const formData = createScrobbleFormData(tracks, apiKey, sk, secret);

    const req = makePostRequest(baseUrl, formData, () => {
        // const res = JSON.parse(req.responseText);
        alert(req.responseText);
    }, () => {
        // const res = JSON.parse(req.responseText);
        alert(req.responseText);
    });
};


// 16 : The service is temporarily unavailable, please try again.
// 2 : Invalid service - This service does not exist
// 3 : Invalid Method - No method with that name in this package
// 4 : Authentication Failed - You do not have permissions to access the service
// 5 : Invalid format - This service doesn't exist in that format
// 6 : Invalid parameters - Your request is missing a required parameter
// 7 : Invalid resource specified
// 8 : Operation failed - Something else went wrong
// 9 : Invalid session key - Please re-authenticate
// 10 : Invalid API key - You must be granted a valid key by last.fm
// 11 : Service Offline - This service is temporarily offline. Try again later.
// 13 : Invalid method signature supplied
// 16 : There was a temporary error processing your request. Please try again
// 26 : Suspended API key - Access for your account has been suspended, please contact Last.fm
// 29 : Rate limit exceeded - Your IP has made too many requests in a short period

const constructSignature = (apiKey: string, method: string, token: string, secret: string) => md5hash(`api_key${apiKey}method${method}token${token}${secret}`);

const sortByKey = (data: string[][]) => data.sort((a, b) => a[0] < b[0] ? -1 : a[0] > b[0] ? 1 : 0);

export const constructSignatureForParams = (data: string[][], secret: string): string => md5hash(
    `${sortByKey(data).map(pair => pair[0] + pair[1])}${secret}`);